﻿<#
    EZ-MBPS-Loader

    AUTHOR: arfonzo <arfonzo@gmail.com>

    - Put it in the Matchbornes.psm1 folder.
    - Put all your translations in a "translations" sub-folder.

    Sample hierarchy:
        translations/*.po
        ezloader.ps1
        Matchbornes.psm1
#>

# Your MatchbornesPS directory path.
$path_matchbornesps = ".\"

# Your translations directory path, with additional wildcard if required.
$path_translations = "$($path_matchbornesps)\translations\*.po"


<#
 # Don't touch below unless you know what you're doing. 
 #>

function ezmbHeader {
    Write-Host ""
    Write-Host "                                   " -BackgroundColor DarkGreen
    Write-Host " m a t c h b o r n e s - EZ Loader " -BackgroundColor DarkGreen -ForegroundColor Yellow
    Write-Host "                                   " -BackgroundColor DarkGreen
    Write-Host ""
}

ezmbHeader

[System.Collections.ArrayList]$matchbornes = @()

$translations = Get-ChildItem -Path "$($path_translations)" -Recurse

Write-Host "Importing Matchbornes module..." -ForegroundColor Green
Import-Module $path_matchbornesps\Matchbornes.psm1

Write-Host "Loading language files..." -ForegroundColor Cyan

$translations | ForEach-Object {
    $matchbornes += Matchbornes -File $_ -Dump
}
Write-Host "Records loaded: $($matchbornes.Count)"

Write-Host "Starting GUI..." -ForegroundColor Magenta
$matchbornes | Out-GridView -Wait

Write-Host "Bye!"
