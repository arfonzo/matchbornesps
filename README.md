# matchbornes (PowerShell flavour!)

## Installation

- You need PowerShell. Version 5.1 or higher for GUI functions (`Out-GridView` support).
- Clone the repository (git clone, or download the raw files).
- Set your PowerShell restriction policies for the downloaded .psm1 and .ps1 (Unblock-File or Set-ExecutionPolicy).

## EZ Loader

`EZ-MBPS-Loader.ps1` has been included for ease of use. You can load multiple translations files into a single matchbornesPS session easily with the loader.

> Example: Load English `original` and `Prepared` translations, and load the Chinese `original` and `Prepared` translations, into a single session.

Start the script in PowerShell, or Open and Run it from PowerShell ISE.

```
<#
    EZ-MBPS-Loader

    AUTHOR: arfonzo <arfonzo@gmail.com>

    - Put it in the Matchbornes.psm1 folder.
    - Put all your translations in a "translations" sub-folder.

    Sample hierarchy:
        translations/*.po
        ezloader.ps1
        Matchbornes.psm1
#>
```

If you use this loader, you won't need to worry about the steps below.

## Basic Usage

Check the tutorial in 3 pictures: https://imgur.com/a/IhZl5gm

### Import the module:

```
Import-Module .\path\to\matchbornesPS\Matchbornes.psm1
```

### Starting GUI mode:

```
Matchbornes -File .\path\to\b2rpg-origin-en-20190823.po -Gui
```

- `-File` parameter is required and should point to a valid translation file.
- `-Gui` parameter is optional, but unless you want to hack around in PowerShell you'll want to use this to output the results in a GUI interface.

### Filter using the GUI

- The GUI mode uses `Out-GridView` which enables spreadsheet-like filtering.
- Use the controls at the top, to filter down to what you're looking for.
- You can type keyword(s) at the top, which uses the logical AND operand.
- You can use the criteria button to do more advanced searches.

## Advanced Usage

The `-Dump` parameter can be used to return a custom PowerShell Object, for you to manipulate manually. 

If you're doing something more low-level or advanced, you may find `-Dump` useful.

Example:

- Dump results in a session object called `$matchbornes`.

```
PS > Import-Module .\Matchbornes.psm1
PS > $matchbornes = Matchbornes -File .\b2rpg-origin-en-20190823.po -Dump


 m a t c h b o r n e s


Loading translation file: .\b2rpg-origin-en-20190823.po
6827 Records found in .\b2rpg-origin-en-20190823.po
```

You can now manipulate the $matchbornes object like any ol' PowerShell Object, neat eh?

```
PS > $matchbornes.Count
6827

```

Do thine standard funky PowerShell shit to it, like pipes and `Where-Object`, etc. That includes manipulating the data, exporting for Excel, anything you want. 

Go nuts!

```
PS > $matchbornes | Where-Object {$_.msgctxt -Match "MasterDesc-ESSE16" -or $_.msgid -Match "ENB0004"}

msgctxt             msgid                                                                msgstr
-------             -----                                                                ------
MasterName-JBM00078 :@MasterName-ENB00040                                                :@MasterName-ENB00040
MasterName-JBM00079 :@MasterName-ENB00041                                                :@MasterName-ENB00041
MasterName-JBM00080 :@MasterName-ENB00042                                                :@MasterName-ENB00042
MasterName-JBM00081 :@MasterName-ENB00043                                                :@MasterName-ENB00043
MasterName-JBM00082 :@MasterName-ENB00044                                                :@MasterName-ENB00044
MasterDesc-JBM00078 :@MasterDesc-ENB00040                                                :@MasterDesc-ENB00040
MasterDesc-JBM00079 :@MasterDesc-ENB00041                                                :@MasterDesc-ENB00041
MasterDesc-JBM00080 :@MasterDesc-ENB00042                                                :@MasterDesc-ENB00042
MasterDesc-JBM00081 :@MasterDesc-ENB00043                                                :@MasterDesc-ENB00043
MasterDesc-JBM00082 :@MasterDesc-ENB00044                                                :@MasterDesc-ENB00044
MasterDesc-ESSE1601 なにかが うごめく にくの かべを つたい はいよってくるのを かんじる。 やつは あなたを みつけた。 もう にげばは どこにもない。 Something is coming along the…
MasterDesc-ESSE1602 :@MasterDesc-ESSE1601                                                :@MasterDesc-MSMAP018
MasterDesc-ESSE1603 :@MasterDesc-ESSE1601                                                :@MasterDesc-MSMAP018
MasterDesc-ESSE1604 :@MasterDesc-ESSE1601                                                :@MasterDesc-MSMAP018

```

You can still start a GUI to show off to your friends, anytime: 

```
$matchbornes | Out-GridView
```

