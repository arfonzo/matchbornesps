﻿<#
    Matchbornes, PowerShell flavour.

    AUTHOR: arfonzo <arfonzo@gmail.com>
#>


function Matchbornes {
    param (
        [Parameter(Mandatory)]
        [String]$File,
        [switch]$Dump = $false,
        [switch]$Gui = $false
    );

    mbHeader

    $translation_file = $File
    [System.Collections.ArrayList]$matchbornes = mbParseTranslationFile($translation_file)

    if ($Gui) {
        Write-Host "Starting GUI interface..."
        $matchbornes | Out-GridView -Title "Matchbornes, PowerShell flavour!"
    }
    
    if ($Dump) {
        # Dumping to object, searching: $matchbornes | Where-Object {$_.msgctxt -Match "MasterDesc-ESSE16" -or $_.msgid -Match "17"}
        return $matchbornes
    }

}

function mbHeader {
    Write-Host ""
    Write-Host "                       " -BackgroundColor DarkMagenta
    Write-Host " m a t c h b o r n e s " -BackgroundColor DarkMagenta -ForegroundColor White
    Write-Host "                       " -BackgroundColor DarkMagenta
    Write-Host ""
}

function mbParseTranslationFile {
    param(
        [string]$TranslationFile
    )

    [System.Collections.ArrayList]$matchbornes = @()

    Write-Host "Loading translation file: $TranslationFile"

    $f = Get-Content -Path $TranslationFile -Encoding UTF8

    For ($i = 0; $i -lt $f.Length; $i++ ) {    

        # `msgctxt` is the key id.
        if ($f[$i].StartsWith("msgctxt")) {
            $msgctxt = $f[$i].TrimStart("msgctxt ").Trim('"')
            $msgid = $f[$i + 1].TrimStart("msgid ").Trim('"')
            $msgstr = $null
            
            For ($a = $i + 2; $f[$a].Length -gt 0; $a++) {
                $msgstr += $f[$a].TrimStart("msgstr ").Trim('"')
                #$i = $a + 1
            } 
            
            #Write-Host "New Item: [$msgctxt] msgid: [$msgid] msgstr:[$msgstr]"

            # Create new Record Object
            $Record = [PSCustomObject]@{
                msgctxt = $msgctxt
                msgid = $msgid
                msgstr = $msgstr
            }

            [void]$matchbornes.Add($Record)
        }

        Write-Progress -Activity "Parsing translation file:" -Status "Line $i of $($f.Length)..." -PercentComplete ((($i+1)/$f.length)*100)
    }

    Write-Host "$($matchbornes.Count) Records found in $TranslationFile"

    return $matchbornes
}